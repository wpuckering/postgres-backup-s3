#! /bin/sh
if [ "${S3_S3V4}" = "true" ]
then
  aws configure set default.s3.signature_version s3v4
fi

if [ "${S3_ACCESS_KEY_ID}" = "" ]
then
  echo "You need to set the S3_ACCESS_KEY_ID environment variable."
  exit 1
fi

if [ "${S3_SECRET_ACCESS_KEY}" = "" ]
then
  echo "You need to set the S3_SECRET_ACCESS_KEY environment variable."
  exit 1
fi

if [ "${S3_BUCKET}" = "" ]
then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi

if [ "${POSTGRES_DB}" = "" ]
then
  echo "You need to set the POSTGRES_DB environment variable."
  exit 1
fi

if [ "${POSTGRES_HOSTNAME}" = "" ]
then
  if [ -n "${POSTGRES_PORT_5432_TCP_ADDR}" ]
  then
    POSTGRES_HOSTNAME=$POSTGRES_PORT_5432_TCP_ADDR
    POSTGRES_PORT=$POSTGRES_PORT_5432_TCP_PORT
  else
    echo "You need to set the POSTGRES_HOSTNAME environment variable."
    exit 1
  fi
fi

if [ "${POSTGRES_USER}" = "" ]
then
  echo "You need to set the POSTGRES_USER environment variable."
  exit 1
fi

if [ "${POSTGRES_PASSWORD}" = "" ]
then
  echo "You need to set the POSTGRES_PASSWORD environment variable."
  exit 1
fi

if [ "${S3_ENDPOINT}" = "" ]
then
  AWS_ARGS=""
else
  AWS_ARGS="--endpoint-url ${S3_ENDPOINT}"
fi

export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

export PGPASSWORD=$POSTGRES_PASSWORD
POSTGRES_HOSTNAME_OPTS="-h $POSTGRES_HOSTNAME -p $POSTGRES_PORT -U $POSTGRES_USER $POSTGRES_EXTRA_OPTS"

echo "Creating dump of ${POSTGRES_DB} database from ${POSTGRES_HOSTNAME}..."

pg_dump $POSTGRES_HOSTNAME_OPTS $POSTGRES_DB | gzip > dump.sql.gz

echo "Uploading dump to bucket $S3_BUCKET"

cat dump.sql.gz | aws $AWS_ARGS s3 cp - s3://$S3_BUCKET/$S3_PREFIX/${POSTGRES_DB}_$(date +"%Y-%m-%dT%H:%M:%SZ").sql.gz $S3_EXTRA_ARGS || exit 2

echo "Database backup uploaded successfully"
exit 0