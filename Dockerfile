FROM alpine:3.9
LABEL maintainer="William Puckering <william.puckering@gmail.com>"

RUN apk add --no-cache \
    python \
    py2-pip \
    postgresql-client=11.2-r0 \
  && pip install awscli \
  && apk del py2-pip

ENV POSTGRES_DB ''
ENV POSTGRES_HOSTNAME ''
ENV POSTGRES_PORT 5432
ENV POSTGRES_USER ''
ENV POSTGRES_PASSWORD ''
ENV POSTGRES_EXTRA_OPTS ''
ENV S3_ACCESS_KEY_ID ''
ENV S3_SECRET_ACCESS_KEY ''
ENV S3_BUCKET ''
ENV S3_REGION 'us-west-1'
ENV S3_PREFIX ''
ENV S3_ENDPOINT ''
ENV S3_S3V4 'true'
ENV S3_EXTRA_ARGS ''

ADD run.sh run.sh

CMD ["sh", "run.sh"]